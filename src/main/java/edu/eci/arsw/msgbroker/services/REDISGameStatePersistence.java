/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.msgbroker.services;

import edu.eci.arsw.msgbroker.model.HangmanGame;
import java.util.Map;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import util.JedisUtil;

/**
 *
 * @author Leonardo Herrera Hugo Alvarez
 */
@Service
public class REDISGameStatePersistence implements GameStatePersistence{

    @Override
    public void createGame(int id, String word) throws GameCreationException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public HangmanGame getGame(int gameid) throws GameNotFoundException {
        boolean ban = false;
        Jedis jedis = JedisUtil.getPool().getResource();
        Map<String,String> hg = jedis.hgetAll("partida:"+gameid);
        if(hg.get("estado").equalsIgnoreCase("si")) ban = true;
        System.out.println(hg.get("palabra")+" "+hg.get("adivinado")+" "+hg.get("ganador")+ " "+hg.get("estado"));
        HangmanGame g = new HangmanGame(hg.get("palabra"),hg.get("adivinado"),hg.get("ganador"),ban);
        jedis.close();
        return g;
    }

    @Override
    public void addLetter(int gameid, char c) throws GameNotFoundException {
        boolean ban = false;
        Jedis jedis = JedisUtil.getPool().getResource();
        Map<String,String> hg = jedis.hgetAll("partida:"+gameid);
        if(hg.get("estado").equalsIgnoreCase("si")) return;
        HangmanGame g = new HangmanGame(hg.get("palabra"),hg.get("adivinado"),hg.get("ganador"),ban);
        g.addLetter(c);
        String guess = g.getCurrentGuessedWord();
        jedis.hset("partida:"+gameid, "adivinado", guess);
        jedis.close();
    }

    @Override
    public boolean checkWordAndUpdateHangman(int gameid, String player, String word) throws GameNotFoundException {
        boolean ban = false;
        Jedis jedis = JedisUtil.getPool().getResource();
        Map<String,String> hg = jedis.hgetAll("partida:"+gameid);
        if(hg.get("estado").equalsIgnoreCase("si")) return true;
        HangmanGame g = new HangmanGame(hg.get("palabra"),hg.get("adivinado"),hg.get("ganador"),ban);
        g.guessWord(player, word);
        if(g.gameFinished()) ban = true;
        jedis.hset("partida:"+gameid, "estado", (ban)?"si":"no");
        jedis.hset("partida:"+gameid, "ganador", (ban)?player:"");
        jedis.hset("partida:"+gameid, "adivinado", g.getCurrentGuessedWord());
        jedis.close();
        return ban;
    }
    
}
